#pragma once
#include "DateTime.h"
#include <iostream>

class TimeInterval
{
	friend class DateTime;

public:
	TimeInterval();
	TimeInterval(int day, int hour, int minute);

	int getDay() { return this->day; }
	int getHour() { return this->hour; }
	int getMinute() { return this->minute; }

	void setDay(int day);
	void setTime(int hour, int minute);

	void print();

private:
	int day;
	int hour;
	int minute;
};

