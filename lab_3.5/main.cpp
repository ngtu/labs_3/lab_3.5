﻿#include <iostream>
#include <fstream>
#include "DateTime.h"
#include "TimeStr.h"
#include "Event.h"
#include "TimeInterval.h"
#include "Stack.h"

int main()
{
    try {
        Stack stack = Stack();
        stack.push(new DateTime(2023, 1, 1, 12, 15));
        stack.push(new TimeStr(2023, 1, 2, 11, 55, true));
        stack.push(new Event(2023, 1, 1, 12, 15, "Event_1"));

        while (!stack.isEmpty()) {
            std::cout << stack.pop()->toStr() << '\n';
        }
    }
    catch (const std::string e) {
        std::cout << "[ERROR]: " << e << '\n';
        return -1;
    }

    return 0;
}