#include "Stack.h"

Stack::Stack()
{
	this->last = nullptr;
	size = 0;
}

Stack::Stack(DateTime* date)
{
	this->last = new Node(date, nullptr);
	this->size = 0;
}

Stack::~Stack()
{
	this->empty();
}

/// <summary>
/// ������� ����
/// </summary>
void Stack::empty()
{
	while (!this->isEmpty()) {
		Node* elem = this->last;
		this->last = this->last->prev;
		delete elem;
	}
}

/// <summary>
/// ���������� ������ � ����
/// </summary>
/// <param name="date">����</param>
void Stack::push(DateTime* date)
{
	Node* elem = new Node(date, this->last);
	this->last = elem;
	this->size++;
}

/// <summary>
/// �������� ������ �� �����
/// </summary>
/// <returns></returns>
DateTime* Stack::pop()
{
	if (this->isEmpty()) {
		throw std::string("Stack is empty");
	}

	Node* elem = this->last;
	DateTime* date = this->last->value;

	this->last = this->last->prev;
	delete elem;

	this->size--;

	return date;
}

Stack::Node::Node(DateTime* date, Node* prev)
{
	this->value = date;
	this->prev = prev;
}
